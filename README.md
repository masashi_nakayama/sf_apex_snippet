# SF_Apex_Snippet

## 概要

Salesforce Apex スニペット
VSCODE設定用のファイル

※アップしているものに限らず、自分で自由に定義を作成できるので、
使いやすいように自分でカスタマイズしてみましょう

## インストール

VisualStudioCode をインストール後、以下拡張機能をインストールしておく

### 前提拡張機能
・Salesforce Extension Pack（Apex, Visualforce などが入ります）
・ForceCode

VSCODEにて、「設定」→「ユーザースニペット」で「Apex」を選択し、
本リポジトリにアップされている、apex.json の内容を挿入する

![Alt text](https://bitbucket.org/masashi_nakayama/sf_apex_snippet/raw/ac7fb58a1b62a0141163da69f8b3e6fda094e4b5/content/user-snippet-menu.jpg)


## 使用例

apex.json の prefix が Hoekeyとなり、エディタ上に候補として表示されます。
※apex.jsonはデフォルトで「.cls」ファイルに定義付けされています

### １．Classを記入後、docテキストを挿入
	使用している HotKey（doc） 
![Alt text](https://bitbucket.org/masashi_nakayama/sf_apex_snippet/raw/07d707282bf62c576aa05b13439de78a3f931e9e/content/doc.gif)

### ２．Listを記述後、デバッグ
	使用している HotKey（l、d） 
![Alt text](https://bitbucket.org/masashi_nakayama/sf_apex_snippet/raw/23a13a0178af10f86ce1c8a48ddfc69885808ecb/content/list.gif)

### ３．Mapを記述後、デバッグ
	使用している HotKey（m、d） 
![Alt text](https://bitbucket.org/masashi_nakayama/sf_apex_snippet/raw/23a13a0178af10f86ce1c8a48ddfc69885808ecb/content/map.gif)

### ４．オブジェクトのレコードタイプを調べる
	使用している HotKey（record） 
![Alt text](https://bitbucket.org/masashi_nakayama/sf_apex_snippet/raw/23a13a0178af10f86ce1c8a48ddfc69885808ecb/content/recordtype.gif)

### ５．SOQL全項目を取得
	使用している HotKey（soql） 
![Alt text](https://bitbucket.org/masashi_nakayama/sf_apex_snippet/raw/23a13a0178af10f86ce1c8a48ddfc69885808ecb/content/soql.gif)

### など…これ以外にも定義があるので使いたい方はどうぞ